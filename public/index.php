<?php
require_once '../services/ReportField.php';
require_once '../connectDB/DB.php';
require_once '../Config.php';


$reportField = new ReportField();
$reportField->setPathFile('../sourse/report.csv');

try {
    $reportField->openFile();
    $reportField->readFile();
} catch (Exception $exception) {
    echo $exception->getMessage();
}



