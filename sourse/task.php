<?php

class ReportFields
{
    const MERCHANT_ID             = 'mid'; // digits only, up to 18 digits
    const MERCHANT_NAME           = 'dba'; // string, max length - 100
    const BATCH_DATE              = 'batch_date'; // YYYY-MM-DD
    const BATCH_REF_NUM           = 'batch_ref_num'; // digits only, up to 24 digits
    const TRANSACTION_DATE        = 'trans_date'; // YYYY-MM-DD
    const TRANSACTION_TYPE        = 'trans_type'; // string, max length - 20
    const TRANSACTION_CARD_TYPE   = 'trans_card_type'; // string, max length - 2, possible values - VI/MC/AX and so on
    const TRANSACTION_CARD_NUMBER = 'trans_card_num'; // string, max length - 20
    const TRANSACTOIN_AMOUNT      = 'trans_amount'; // amount, negative values are possible
}

$mapping = [
    ReportFields::TRANSACTION_DATE        => 'Transaction Date',
    ReportFields::TRANSACTION_TYPE        => 'Transaction Type',
    ReportFields::TRANSACTION_CARD_TYPE   => 'Transaction Card Type',
    ReportFields::TRANSACTION_CARD_NUMBER => 'Transaction Card Number',
    ReportFields::TRANSACTOIN_AMOUNT      => 'Transaction Amount',
    ReportFields::BATCH_DATE              => 'Batch Date',
    ReportFields::BATCH_REF_NUM           => 'Batch Reference Number',
    ReportFields::MERCHANT_ID             => 'Merchant ID',
    ReportFields::MERCHANT_NAME           => 'Merchant Name',
];

/**
 * CSV report:
 * - the first line contains headers always
 * - you should ensure that all required fields are present
 * - columns order is unknown
 * - file contains a list of transactions
 * - batch's transactions are always stored together
 *
 * Merchant1 (key - MERCHANT_ID)
 *      Batch1 (key - BATCH_DATE & BATCH_REF_NUM)
 *          Transaction1
 *          Transaction2
 *      Batch2
 *          Transaction3
 *          Transaction4
 * Merchant2 (key - MERCHANT_ID)
 *      Batch3 (key - BATCH_DATE & BATCH_REF_NUM)
 *          Transaction5
 *          Transaction6
 *
 * Your class:
 * - will receive a file name (with full path) and mappings (like $mapping)
 * - should be able to import a given file (if all required headers are present)
 * - suggest a db structure and write SQL commands to create it
 * - be able to process big files with low enough memory usage
 *
 * Use cases (just prepare SQL queries for these cases):
 * - display all transactions for a batch (merchant + date + ref num)
 *      date, type, card_type, card_number, amount
 * - display stats for a batch
 *   per card type (VI - 2 transactions with $100 total, MC - 10 transaction with $200 total)
 * - display stats for a merchant and a given date range
 * - display top 10 merchants (by total amount) for a given date range
 *      merchant id, merchant name, total amount, number of transactions
 */

/*
Отчет CSV:
 * - первая строка содержит заголовки всегда
 * - убедитесь, что все обязательные поля присутствуют
 * - порядок столбцов неизвестен
 * - файл содержит список транзакций
 * - пакетные транзакции всегда хранятся вместе
 *
 * Merchant1 (ключ - MERCHANT_ID)
 *  Пакет 1 (ключ - BATCH_DATE & BATCH_REF_NUM)
 *      Transaction1
 *      Транзакция2
 *  Пакет2
 *      Транзакция3
 *      Transaction4
 * Merchant2 (ключ - MERCHANT_ID)
 *  Пакет 3 (ключ - BATCH_DATE & BATCH_REF_NUM)
 *      Транзакция5
 *      Транзакция6
 *
 * Твой класс:
 * - получит имя файла (с полным путем) и сопоставления (например, $ mapping)
 * - должен иметь возможность импортировать данный файл (если присутствуют все необходимые заголовки)
 * - предложить структуру БД и написать команды SQL для ее создания
 * - иметь возможность обрабатывать большие файлы с достаточно низким использованием памяти
 *
 * Варианты использования (просто подготовьте SQL-запросы для этих случаев):
 *  - отобразить все транзакции для партии (продавец + дата + номер заказа)
 * дата, тип, номер карты, номер карты, сумма
 *  - отображение статистики по партии
 * по типу карты (VI - 2 транзакции на общую сумму 100 долларов США, MC - 10 транзакций на общую сумму 200 долларов США)
 *  - отображать статистику по торговцу и заданному диапазону дат
 *  - отображать 10 лучших торговцев (по общей сумме) за указанный диапазон дат
 * идентификатор продавца, имя продавца, общая сумма, количество транзакций
*/
