<?php

class ServiceImport
{
    /** @var array string */
    protected $array;

    /** @var Merchant */
    protected $merchantModel;

    /** @var TransType */
    protected $transTypeModel;

    /** @var Reference */
    protected $referNumberModel;

    /** @var array */
    protected $configMapping = Config::MAPPING;

    /**
     * ServiceImport constructor.
     */
    public function __construct()
    {
        $this->merchantModel = new Merchant();
        $this->transTypeModel = new TransType();
        $this->referNumberModel = new Reference();
    }

    /**
     * @param array $array
     */
    public function setArray(array $array)
    {
        $this->array = $array;
    }

    public function saveAll()
    {

        $this->saveMerchant();
        $this->saveTransType();
        $this->saveReferenceNumber();


        var_dump($this->merchantModel->getMerchantID());
    }

    /**
     * @return bool
     */
    protected function saveMerchant(): bool
    {
        $merchant = $this->array[$this->configMapping[Config::MERCHANT_ID]];
        if ($merchant == $this->merchantModel->getMerchant())
            return false;

        $this->merchantModel->setMerchant($merchant);
        $this->merchantModel->save();

        return true;
    }

    protected function saveTransType(): bool
    {
        $transType = $this->array[$this->configMapping[Config::TRANSACTION_TYPE]];
        $this->transTypeModel->setTransType($transType);
        if ($this->transTypeModel->checkTransType())
            return false;

        $this->transTypeModel->save();

        return true;
    }

    protected function saveReferenceNumber(): bool
    {
        $referenceNumber = $this->array[$this->configMapping[Config::BATCH_REF_NUM]];
        if ($referenceNumber == $this->referNumberModel->getRefer())
            return false;

        $this->referNumberModel->setRefer($referenceNumber);
        $this->referNumberModel->save();

        return true;
    }
}