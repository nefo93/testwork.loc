<?php

class ServiceValidation
{
    public function checkMapping(array $data):bool
    {
        foreach (Config::MAPPING as $value) {
            if(!in_array($value, $data)) {
                return true;
            }
        }

        return false;
    }
}