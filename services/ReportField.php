<?php
require_once 'serviceValidation/ServiceValidation.php';
require_once 'serviceImport/ServiceImport.php';
require_once '../model/Merchant.php';
require_once '../model/TransType.php';
require_once '../model/Reference.php';

class ReportField
{
    /** @var string */
    protected $path;

    /** @var resource report.csv */
    protected $file;

    /** @var ServiceValidation */
    protected $validation;

    /** @var ServiceImport */
    protected $serviceImport;

    public function __construct()
    {
        $this->validation = new ServiceValidation();
        $this->serviceImport = new ServiceImport();
    }

    /**
     * @param string $path
     */
    public function setPathFile(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param string|null $path
     * @return bool|resource
     * @throws Exception
     */
    public function openFile(string $path = null)
    {
        if ($path)
            return $this->file = fopen($path, 'r');

        if (!$this->path)
            throw new Exception('error not file open');

        return $this->file = fopen($this->path, 'r');
    }

    /**
     * @throws Exception
     */
    public function readFile()
    {
        $flag = true;
        while ($string = fgetcsv($this->file)) {
            if ($flag) {
                if ($this->validation->checkMapping($string)) {
                    throw new Exception('file does not match mapping');
                }
                $mapping = $string;
                $flag = false;
                continue;
            }
            $string = array_combine($mapping, $string);
            $this->serviceImport->setArray($string);
            $this->serviceImport->saveAll();
//            break;
        }
    }

    /**
     * get string in csv file
     * @return array
     */
    protected function getString(): array
    {
        return fgetcsv($this->file);
    }

    /**
     * close file
     */
    public function __destruct()
    {
        fclose($this->file);
    }
}