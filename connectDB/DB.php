<?php
require_once '../Config.php';

class DB  {
    /** @var DB */
    private static $instance;
    /** @var PDO  */
    private $conn;

    private function __construct()
    {
        $this->conn = new PDO(
            'mysql:host='.Config::DB_HOST.';port='.Config::DB_PORT.';dbname='.Config::DB_NAME,
            Config::DB_USER,
            Config::DB_PASS
        );

    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new DB();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->conn;
    }
}