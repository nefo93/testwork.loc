<?php

class TransType
{
    const TABLE_NAME = "trans_type";

    /**
     * @var PDO
     */
    protected $DB;

    public function __construct()
    {
        $this->DB = DB::getInstance()->getConnection();
    }

    public function createTable():void
    {
        $sql = "CREATE TABLE ".self::TABLE_NAME. "(
            id INT AUTO_INCREMENT PRIMARY KEY,
            trans_type varchar(20)
        )";

        $this->DB->query($sql);
    }

    public function checkTable()
    {
        $sql = "SHOW TABLES LIKE '".self::TABLE_NAME."'";

        return $this->DB->query($sql)->fetch();
    }
}