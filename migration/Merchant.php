<?php

class merchant
{
    const TABLE_NAME = "merchant";

    /**
     * @var PDO
     */
    protected $DB;

    public function __construct()
    {
        $this->DB = DB::getInstance()->getConnection();
    }

    public function createTable():void
    {
        $sql = "CREATE TABLE ".self::TABLE_NAME. "(
            id INT AUTO_INCREMENT PRIMARY KEY,
            mid DECIMAL(18,0)
        )";

        $this->DB->query($sql);
    }

    public function checkTable()
    {
        $sql = "SHOW TABLES LIKE '".self::TABLE_NAME."'";

        return $this->DB->query($sql)->fetch();
    }
}