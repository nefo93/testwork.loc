<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 28.04.19
 * Time: 0:26
 */

class BatchReferenNumber
{
    const TABLE_NAME = "reference";

    /**
     * @var PDO
     */
    protected $DB;

    public function __construct()
    {
        $this->DB = DB::getInstance()->getConnection();
    }

    public function createTable():void
    {
        $sql = "CREATE TABLE ".self::TABLE_NAME. "(
            id INT AUTO_INCREMENT PRIMARY KEY,
            batch_ref_num DECIMAL(24,0) 
        )";

        $this->DB->query($sql);
    }

    public function checkTable()
    {
        $sql = "SHOW TABLES LIKE '".self::TABLE_NAME."'";

        return $this->DB->query($sql)->fetch();
    }
}