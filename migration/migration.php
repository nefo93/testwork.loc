<?php

require_once '../connectDB/DB.php';
require_once 'Merchant.php';
require_once 'BatchReferenNumber.php';
require_once 'TransType.php';

$merchant = new Merchant();
$referen = new BatchReferenNumber();
$trans_type = new TransType();

if ($merchant->checkTable()) {
    echo "\033[0;41m"."Table 'Merchant' exists "."\033[0m"."\n";
} else {
    $merchant->createTable();
    echo "\033[0;32m"."Table 'Merchant' create "."\033[0m"."\n";
}

if ($referen->checkTable()) {
    echo "\033[0;41m"."Table 'reference' exists "."\033[0m"."\n";
} else {
    $referen->createTable();
    echo "\033[0;32m"."Table 'reference' create "."\033[0m"."\n";
}

if ($trans_type->checkTable()) {
    echo "\033[0;41m"."Table 'trans_type' exists "."\033[0m"."\n";
} else {
    $trans_type->createTable();
    echo "\033[0;32m"."Table 'trans_type' create "."\033[0m"."\n";
}