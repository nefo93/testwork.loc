<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 29.04.19
 * Time: 11:59
 */

class TransType
{
    const TABLE = 'trans_type';
    const TRANS_TYPE = Config::TRANSACTION_TYPE;

    /** @var PDO */
    protected $db;

    /** @var string */
    protected $transType = '';

    /** @var string */
    protected $transTypeID = '';

    public function __construct()
    {
        $this->db = DB::getInstance()->getConnection();
    }

    /**
     * @param string $transType
     */
    public function setTransType(string $transType): void
    {
        $this->transType = $transType;
    }

    /**
     * @return string|null
     */
    public function checkTransType()
    {
        $sql = "SELECT * FROM ".self::TABLE." WHERE ".self::TRANS_TYPE." = '".$this->transType."'";
        $transType = $this->db->query($sql)->fetch();
        $this->transTypeID = $transType[0];
        return $this->transTypeID;
    }

    /**
     * @return string
     */
    public function getTransTypeID(): string
    {
        return $this->transTypeID;
    }

    public function save(): void
    {
        $sql = "INSERT INTO ".self::TABLE."(".self::TRANS_TYPE.") VALUES ('".$this->transType."')";
        $this->db->exec($sql);
        $this->transTypeID = $this->db->lastInsertId();
    }
}