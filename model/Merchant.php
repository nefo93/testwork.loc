<?php

class Merchant
{
    const TABLE = 'merchant';
    const MID = Config::MERCHANT_ID;

    /** @var PDO */
    protected $db;

    /** @var string */
    protected $merchant = '';

    /** @var string */
    protected $merchantID = '';

    public function __construct()
    {
        $this->db = DB::getInstance()->getConnection();
    }

    /**
     * @param string $merchant
     */
    public function setMerchant(string $merchant): void
    {
        $this->merchant = $merchant;
    }

    /**
     * @return string
     */
    public function getMerchant(): string
    {
        return $this->merchant;
    }

    public function getMerchantID(): string
    {
        return $this->merchantID;
    }

    public function save()
    {
        $sql = "INSERT INTO ".self::TABLE."(".self::MID.") VALUES (".$this->merchant.")";
        $this->db->exec($sql);
        $this->merchantID = $this->db->lastInsertId();
    }
}