<?php

class Reference
{
    const TABLE = 'reference';
    const BATCH_REF_NUM = Config::BATCH_REF_NUM;

    /** @var PDO */
    protected $db;

    /** @var string */
    protected $refer;

    /** @var string */
    protected $referID;


    public function __construct()
    {
        $this->db = DB::getInstance()->getConnection();
    }

    /**
     * @param string $refer
     */
    public function setRefer(string $refer)
    {
        $this->refer = $refer;
    }

    /**
     * @return string|null
     */
    public function getRefer()
    {
        return $this->refer;
    }

    /**
     * @return string
     */
    public function getReferID(): string
    {
        return $this->referID;
    }

    public function save(): void
    {
        $sql = "INSERT INTO ".self::TABLE."(".self::BATCH_REF_NUM.") VALUES (".$this->refer.")";
        $this->db->exec($sql);
        $this->referID = $this->db->lastInsertId();
    }
}